using System.ComponentModel.DataAnnotations;

public class InputCheck
{
    [Required]
    [StringLength(10, ErrorMessage = "too long")]
    public string Name {get; set;}
    public string Route {get; set;}
    public string RESTTyp {get; set;}
    public string ConfigTyp {get; set;}
}