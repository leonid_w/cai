using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Layour.Shared;

namespace Layour.Client.Services
{
    public interface IDataService
    {
        event Action OnChange;

        List<Data> Daten {get; set;}        
        Task<List<Data>> GetData();

        Task<Data> GetSingleData(int id);

        Task<List<Data>> CreateData(Data data);
        
    }
}