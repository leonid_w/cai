using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Layour.Shared;

namespace Layour.Client.Services
{

public interface IMenuService 
{
    List<Data> MenuItems {get; set;}
    event EventHandler<EventArgs> OnChanged;
    void NotifyChanged();
}
}