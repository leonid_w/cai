using System;
using Layour.Shared;
using System.Net.Http;
using System.Net.Http.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Layour.Client.Services
{
    public class DataService : IDataService
    {
        private readonly HttpClient _httpClient;

        public List<Data> Daten {get; set;} = new List<Data>();

        public DataService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<List<Data>> CreateData(Data data)
        {
            var result = await _httpClient.PostAsJsonAsync($"api/details", data);
            Daten = await result.Content.ReadFromJsonAsync<List<Data>>();          
            OnChange.Invoke();
            return Daten;
        }

        public event Action OnChange;

        public async Task<Data> GetSingleData(int id)
        {
            return await _httpClient.GetFromJsonAsync<Data>($"api/details/{id}");
        }

        public async Task<List<Data>> GetData()
        {
            Daten = await _httpClient.GetFromJsonAsync<List<Data>>("api/details");
            return Daten;

        }


    }
}