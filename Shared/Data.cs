using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Layour.Shared
{
    public class Data
    {
        public int Id {get; set;}
        [Required]
        [StringLength(9, ErrorMessage = "Zu Lang")]
        public string Name {get; set;}
        public string Route {get; set;}
        public string RESTTyp {get; set;}
        public string ConfigTyp {get; set;}
    }
}

