using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Layour.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Layour.Server.Controllers
{


[Route("api/[controller]")]
[ApiController]
public class DetailsController : ControllerBase
{

    private static List<Data> MData = new List<Data>
    {
        new Data {Id=1, Name = "test", Route = "ROuteTest", RESTTyp = "RESTTYP", ConfigTyp = "GET"},
        new Data {Id=2, Name = "2test", Route = "2ROuteTest", RESTTyp = "2RESTTYP", ConfigTyp = "2GET"}
    };

    [HttpGet]
    public async Task<IActionResult> GetData()
    {
        return Ok(MData);
    }

    [HttpGet("{id}")]

    public async Task<IActionResult> GetSingleData(int id)
    {
        var data = MData.FirstOrDefault(h => h.Id == id);
        if(data == null)
            return NotFound("Data wasn't found");

        return Ok(data);
    }

    [HttpPost]
    public async Task<IActionResult> CreateData(Data data)
    {
        MData.Add(data);
        Console.WriteLine($"{MData.Count}POSTTest");
        data.Id++;

        return Ok(MData);
    }
}
}






